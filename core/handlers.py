import requests
from bidict import bidict
from flask_assistant import tell, ask, context_manager

from core.dialogflow_util import get_user_id, ssml, add_default_suggestions, \
    build_fact_response, build_question_response, add_answer_suggestions
from core.firebase import get_user, create_user, get_user_reference

CATEGORIES = {'culture', 'environment', 'transportation'}
ANSWER_MAP = bidict({'A': 0, 'B': 1, 'C': 2})


def default_fallback_handler():
    r = ask('Sorry; I don\'t understand, but I\'m still learning. Maybe '
            'I\'ll be able to help you with that in the future. Please ask '
            'me something else.')
    add_default_suggestions(r)
    return r


def default_welcome_handler():
    user_id = get_user_id()
    print('USER ID: {}'.format(get_user(user_id)))
    user = get_user(user_id)
    print('USER: {}'.format(user))
    if user is None:
        create_user(user_id)
        user = get_user(user_id)
        print('CREATED USER: {}'.format(user))
    r = ask('Welcome to Montreal Genius! I tell tons of facts about Montreal '
            'and ask you trivia questions about them. What would you like to '
            'do?')
    add_default_suggestions(r)
    return r


def help_handler():
    r = ask('I can tell you tons of facts about Montreal and make you '
            'play some trivia related to them.')
    add_default_suggestions(r)
    return r


def fact_handler(category):
    print('CATEGORY: {}'.format(category))
    cat = category if category in CATEGORIES else 'all'
    fact_obj = requests.get(
        url='https://eiiaor3yn1.execute-api.us-east-1.amazonaws.com/prod/facts',
        params={'category': cat, 'n': 1}
    ).json()[0]
    get_user_reference(get_user_id()).update({
        'currentContext': 'fact',
        'currentCategory': cat,
        'currentObject': fact_obj
    })
    return build_fact_response(fact_obj)


def question_handler(category):
    print('CATEGORY: {}'.format(category))
    cat = category if category in CATEGORIES else 'all'
    question_obj = requests.get(
        url='https://eiiaor3yn1.execute-api.us-east-1.amazonaws.com/prod'
            '/questions',
        params={'category': cat, 'n': 1}
    ).json()[0]
    get_user_reference(get_user_id()).update({
        'currentContext': 'question',
        'currentCategory': cat,
        'currentObject': question_obj,
        'answered': False
    })
    return build_question_response(question_obj)


def answer_handler(answer):
    if answer == '':
        r = ask('You should answer with either a), b), or c).')
        add_answer_suggestions(r)
        return r
    user = get_user(get_user_id())
    if 'answered' in user and user['answered'] is True:
        r = ask('You have already answered; ask me for another question.')
        add_default_suggestions(r)
        return r
    answer_id = ANSWER_MAP[answer]
    question_obj = get_user(get_user_id())['currentObject']
    correct_answer = question_obj['answer']
    if answer_id == correct_answer:
        speech = 'Congratulations! You got it! I\'ll find you a tougher one ' \
                 'next time!'
    else:
        speech = 'Aww, you were close, but that\'s not it. The correct ' \
                 'answer was {})<break time="250ms"/> {}. I\'m sure you\'ll ' \
                 'get it next time. '.format(
                    ANSWER_MAP.inv[correct_answer].lower(),
                    question_obj['answer_choices'][correct_answer])
    get_user_reference(get_user_id()).update({'answered': True})
    r = ask(ssml(speech))
    add_default_suggestions(r)
    return r


def tell_another_handler():
    context_manager.add('REPEAT')
    print(context_manager.get('REPEAT'))
    user = get_user(get_user_id())
    category = user['currentCategory']
    if user['currentContext'] == 'fact':
        return fact_handler(category)
    else:
        context_manager.add('ANSWER')
        print(context_manager.get('ANSWER'))
        return question_handler(category)


def repeat_handler():
    user = get_user(get_user_id())
    if user['currentContext'] == 'fact':
        return build_fact_response(user['currentObject'])
    else:
        return build_question_response(user['currentObject'])


def end_of_discussion_handler():
    return tell('Goodbye; take care!')
