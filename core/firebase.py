import firebase_admin
from firebase_admin.credentials import Certificate
from firebase_admin import db

firebase_admin.initialize_app(
    Certificate('./core/montrealgenius-firebase-sa-key.json'),
    {'databaseURL': 'https://montrealgenius.firebaseio.com/'})


def get_user_reference(user_id, path=None):
    uid = 'users/{}'.format(user_id)
    ref_path = '/'.join([uid, path]) if path is not None else uid
    return db.reference(ref_path)


def get_user(user_id):
    return get_user_reference(user_id).get()


def create_user(user_id):
    get_user_reference(user_id).update({'userId': user_id})
