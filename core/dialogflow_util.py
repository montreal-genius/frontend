import dotmap
from flask import request
from flask_assistant import ask


def get_req_obj():
    return dotmap.DotMap(request.get_json(silent=True, force=True))


def get_user_id():
    req = get_req_obj()
    return req.originalRequest.data.user.userId


def device_has_screen():
    req = get_req_obj()
    caps = req.originalRequest.data.surface.capabilities
    return any(c.name == 'actions.capability.SCREEN_OUTPUT' for c in caps)


def ssml(a):
    return '<speak>{}</speak>'.format(a)


def add_default_suggestions(r):
    r.suggest('Tell a fact', 'Ask trivia question', 'Help')


def add_answer_suggestions(r):
    r.suggest('a)', 'b)', 'c)')


def build_fact_response(fact_obj):
    speech = 'Alright! Here\'s an interesting fact! {}'.format(
        fact_obj['fact'])
    r = ask(speech)
    add_default_suggestions(r)
    if 'title' in fact_obj and 'fact' in fact_obj and 'image' in fact_obj:
        r.card(
            title=fact_obj['title'],
            text=fact_obj['fact'],
            img_url=fact_obj['image'],
            img_alt=fact_obj['title'],
            link=fact_obj['link'] if 'link' in fact_obj else None,
            linkTitle='LEARN MORE' if 'link' in fact_obj else None
        )
    return r


def build_question_response(question_obj):
    speech = 'Alright; here it goes. {} <break time="650ms"/>'.format(
        question_obj['question'])
    anss = ['{})<break time="250ms"/> '.format(a) for a in ('a', 'b', 'c')]
    speech += '<break time="450ms"/>'.join(
        [' {} {}'.format(r, answer_choice)
         for r, answer_choice
         in zip(anss, question_obj['answer_choices'])])
    r = ask(ssml(speech))
    add_answer_suggestions(r)
    return r
