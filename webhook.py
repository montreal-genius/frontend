from flask import Flask
from flask_assistant import Assistant, context_manager

from core.handlers import default_fallback_handler, default_welcome_handler, \
    help_handler, fact_handler, question_handler, answer_handler, \
    end_of_discussion_handler, tell_another_handler, repeat_handler

APP = Flask(__name__)
APP.config['ASSIST_ACTIONS_ON_GOOGLE'] = True
CTX_MGR = context_manager
ASSISTANT = Assistant(APP, '/')


@ASSISTANT.action(intent_name='Default Fallback Intent', is_fallback=True)
def default_fallback_intent():
    return default_fallback_handler()


@ASSISTANT.action(intent_name='Default Welcome Intent', events=['WELCOME'])
def default_welcome_intent():
    return default_welcome_handler()


@ASSISTANT.action(intent_name='Help Intent')
def help_intent():
    return help_handler()


@ASSISTANT.action(intent_name='Fact Intent')
def fact_intent(category):
    return fact_handler(category)


@ASSISTANT.action(intent_name='Question Intent')
def question_intent(category):
    return question_handler(category)


@ASSISTANT.action(intent_name='Answer Intent')
def answer_intent(answer):
    return answer_handler(answer)


@ASSISTANT.action(intent_name='Tell Another Intent')
def tell_another_intent():
    return tell_another_handler()


@ASSISTANT.action(intent_name='Repeat Intent')
def repeat_intent():
    return repeat_handler()


@ASSISTANT.action(intent_name='End of Discussion Intent')
def end_of_discussion_intent():
    return end_of_discussion_handler()


if __name__ == '__main__':
    APP.run(debug=True, port=5010)
